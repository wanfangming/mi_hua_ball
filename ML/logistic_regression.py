# -*- coding: utf-8 -*-
# @Time    : 2021/5/14 15:29
# @Author  : 万方名
# @FileName: logistic_regression.py


from sklearn import linear_model

# 加载线性回归模型
reg = linear_model.LinearRegression()

# 训练数据， 注意格式，自己根据这些格式做数据很方便的
train_X = [[0, 0], [1, 1], [2, 2]]
train_y = [0, 1, 2]
reg.fit(train_X, train_y)

# 打印学习到的参数（w）
print(f'学习到的参数：{reg.coef_}')

# 预测
# 这里也要注意用来预测的格式，需要两对括号，一对会报维度错误，因为输入到模型的X是n*2的维度，两对括号是1*2.KNN，但一对是2，所以不对
result = reg.predict([[3, 3]])
print(result)
