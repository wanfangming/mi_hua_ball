# -*- coding: utf-8 -*-
# @Time    : 2021/6/7 13:47
# @Author  : 万方名
# @FileName: 遍历文件夹中所有文件.py

import os

g = os.walk(r"data/abs")

for path, dir_list, file_list in g:
    for file_name in file_list:
        file_path = os.path.join(path, file_name)

