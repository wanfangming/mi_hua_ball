# -*- coding: utf-8 -*-
# @Time    : 2021/6/16 9:45
# @Author  : 万方名
# @FileName: json的读写.py

import json

# 写入整个字典
d = {'a': 1, 'b': 2}
file = open('../data/json_test.json', 'w')
json.dump(d, file)
file.close()

# 读取整个字典
file_r = open('../data/json_test.json', 'r')
d_r = json.load(file_r)

# json按行写文件
data = [
    {'id': '123456', 'text': ' python'},
    {'id': '456789', 'text': ' java'}
]

for item in data:
    with open('data.json', 'a+', encoding='utf-8') as f:
        line = json.dumps(item, ensure_ascii=False)
        f.write(line + '\n')

############

# json按行读文件
data = []
with open('/data/track_tlassification/tacd/all_tacd.json', 'r', encoding="utf-8") as f:
    # 读取所有行 每行会是一个字符串
    for j in f.readlines():
        print(j)
        print(type(j))
        # 将josn字符串转化为dict字典
        j = json.loads(j)
        print(type(j))
        data.append(j)
print(data)
